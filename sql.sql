drop database if exists films_db;
create database if not exists films_db;

use films_db;
create table if not exists film (
	id int primary key,
    name varchar(255) not null,
    duration double not null,
    genre varchar(100),
    year int
);

create table if not exists cinema (
	id int primary key,
    name varchar(100) not null,
	address varchar(200) not null unique,
    cinemaHallCount int
);


insert into film value (1, "Веном", 134, "Фантастика", 2018);
insert into film (id, name, genre, year, duration) value (2, "Побег из Шоушенка", "Драма", 1994, 165);
insert into film values 
	(3, "Звездные воины", 143, "Фантастика", 1980),
	(4, "Властелин колец", 195, "Фэнтези", 2003),
	(5, "Криминальное чтиво", 178, "Криминальнал", 1994);

update film set duration = 154 where id = 5;

select * from film;
select * from film where name = "Криминальное чтиво";
select name, genre from film where year >= 2000;

select * from film where genre like "Фа%";

delete from film where year >= 2010;
select * from film;

