package films.hibernate.repository.impl;

import hibernate.domain.Film;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import java.util.Properties;

public class TestHibernateConfiguration {
    private static SessionFactory factory;

    static {
        Properties properties = new Properties();
        properties.setProperty("hibernate.connection.driver", "org.h2.Driver");
        properties.setProperty("hibernate.connection.url", "jdbc:h2:mem:film_test;INIT=CREATE SCHEMA IF NOT EXISTS filmtest");
        properties.setProperty("hibernate.connection.username", "sa");
        properties.setProperty("hibernate.connection.password", "");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        properties.setProperty("hibernate.show_sql", "true");
        properties.setProperty("hibernate.format_sql", "true");

        ServiceRegistry registry = new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .build();

        Configuration configuration = new Configuration()
                .addAnnotatedClass(Film.class);

        factory = configuration.buildSessionFactory(registry);


    }

    public static SessionFactory getFactory() {
        return factory;
    }
}
