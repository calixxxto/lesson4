package films.hibernate.repository.impl;

import hibernate.domain.Film;
import hibernate.repository.FilmRepository;
import hibernate.repository.impl.FilmRepositoryImpl;
import org.hibernate.SessionFactory;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FilmRepositoryImplIT {

    private static SessionFactory factory;
    private FilmRepository filmRepository;

    @BeforeClass
    public static void setupFactory(){
        factory = TestHibernateConfiguration.getFactory();
    }

    @Before
    public void setup(){
        this.filmRepository = new FilmRepositoryImpl(factory);
    }

    @Test
    public void testCreate(){
        int id = 100;
        String name = "film name";
        double duration = 165.4d;
        String genre = "genre";
        int year = 2012;

        filmRepository.create(id, name, duration, genre, year);
        Film filmFromDB = filmRepository.getById(id);

        assertEquals(id, filmFromDB.getId());
        assertEquals(name, filmFromDB.getName());
        assertEquals(duration, filmFromDB.getDuration(),0d);
        assertEquals(genre, filmFromDB.getGenre());
        assertEquals(year, filmFromDB.getYear());
    }

    @Test
    public void testGetById(){
        int id = 101;
        filmRepository.create(id, "", 0d, "", 0);
        Film film = filmRepository.getById(id);
        assertNotNull(film);
    }

    @AfterClass
    public static void closeFactory(){
        factory.close();
    }
}
