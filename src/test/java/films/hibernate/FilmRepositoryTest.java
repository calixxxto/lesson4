package films.hibernate;

import hibernate.Validator;
import hibernate.domain.Film;
import hibernate.repository.FilmRepository;
import hibernate.repository.impl.FilmRepositoryImpl;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FilmRepositoryTest {

    @Mock
    private SessionFactory sessionFactory;
//    @Mock
//    private Validator validator;

    @Mock
    private Transaction transaction;
    @Mock
    private Session session;

    private FilmRepository repository;

    @Before
    public void setup(){
//        MockitoAnnotations.initMocks(this);
//        sessionFactory = Mockito.mock(SessionFactory.class);
//        System.out.println(sessionFactory.getClass().getSimpleName());
        Mockito.when(sessionFactory.openSession())
                .thenReturn(session);

        Mockito.when(session.beginTransaction())
                .thenReturn(transaction);

        repository = new FilmRepositoryImpl(sessionFactory);
    }

    @Test
    public void testCreate(){

        int id = 1;
        String filmName = "film name";
        double duration = 156;
        Film film = repository.create(1, filmName, duration, "", 0);

        Assert.assertEquals(id, film.getId());
        Assert.assertEquals(filmName, film.getName());
        Assert.assertEquals(duration, film.getDuration(), 0.0d);

        Mockito.verify(session).persist(Matchers.any());
        Mockito.verify(transaction).commit();
        Mockito.verify(session).close();

    }

//    @Test(expected = IllegalArgumentException.class)
//    public void testCreate_invalidName_throwException(){
//        Mockito.doThrow(IllegalArgumentException.class).when(validator).checkNotEmpty(null);
//        repository.create(1, null, 0, "", 0);
//    }
}
