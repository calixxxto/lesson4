package javaconfig.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "films")
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "film_id_generator")
    @SequenceGenerator(name = "film_id_generator", sequenceName = "film_id_seq",
            initialValue = 1000)
    private int id;
    // uuid, guid
    // increment
    // assignment

    @OneToOne
    @JoinColumn(name = "details_id")
    private FilmDetails details;

    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FilmDetails getDetails() {
        return details;
    }

    public void setDetails(FilmDetails details) {
        this.details = details;
    }

}
