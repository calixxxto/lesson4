package javaconfig.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;


@Embeddable
public class Passport implements Serializable {

    private String serial;
    private String number;

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Passport() {
    }

    public Passport(String serial, String number) {
        this.serial = serial;
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Passport)) return false;

        Passport other = (Passport) o;

        return Objects.equals(serial, other.serial)
                && Objects.equals(number, other.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serial, number);
    }
}
