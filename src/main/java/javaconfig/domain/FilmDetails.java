package javaconfig.domain;

import javax.persistence.*;

@Entity
@Table(name = "film_details")
public class FilmDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "details_id")
    private int id;

    private double duration;

    public FilmDetails() {
    }

    public FilmDetails(double duration) {
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
}
