package javaconfig;

import javaconfig.domain.Film;
import javaconfig.domain.FilmDetails;
import javaconfig.domain.Passport;
import javaconfig.domain.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class App {

    public static void main(String[] args) {
        HibernateConfiguration configuration = new HibernateConfiguration();
        configuration.configure();
        SessionFactory factory = configuration.getFactory();

        // interfaces from java.util.function package
        // Supplier - ничего не принимает, но что то возвращает
        // Consumer - ничего не возвращает, но что то принимает
        // Function - что то возвращает и что то принимает
        // BiFunction - принимает 2 параметра и возвращает один в основном мапы
        // BiConsumer - принимает 2 параметра и ничего не возвращает в основном мапы
        // Predicate - передается элемент, возвращается булеан

        save(factory, ((tx, session) -> {
            Film film = new Film();
            film.setName("Hobbit");
            session.persist(film);
            tx.commit();
        }));

        save(factory, (tx, session) -> {
            Film film = new Film();
            film.setName("Star Wars");

            session.persist(film);

            FilmDetails details = new FilmDetails(186);
            session.persist(details);
            film.setDetails(details);

            tx.commit();

//            System.out.println(film.getId());
        });

        save(factory, (tx, session) -> {
            Passport passport = new Passport("3034", "643221");
            User user = new User(passport, "Ivan", "Sidorov");
        });

        factory.close();
    }

    private static void save(SessionFactory factory, BiConsumer consumer) {
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        consumer.accept(tx, session);

        session.close();
    }

    @FunctionalInterface
    private static interface BiConsumer {
        void accept(Transaction tx, Session session);
    }
}
