package relations;


import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import relations.domain.Award;
import relations.domain.Cinema;
import relations.domain.Details;
import relations.domain.Film;

import java.util.Properties;

public class HibernateConfiguration {
    private SessionFactory factory;
    public HibernateConfiguration() {
    }

    public void configure() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.connection.driver", "com.mysql.cj.jdbc.Driver");
        properties.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/films_test?useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true");
        properties.setProperty("hibernate.connection.username", "root");
        properties.setProperty("hibernate.connection.password", "root");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect");
        properties.setProperty("hibernate.hbm2ddl.auto", "create");
        properties.setProperty("hibernate.show_sql", "true");
        properties.setProperty("hibernate.format_sql", "true");

        ServiceRegistry registry = new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .build();

        Configuration configuration = new Configuration()
                .addAnnotatedClass(Film.class)
                .addAnnotatedClass(Details.class)
                .addAnnotatedClass(Award.class)
                .addAnnotatedClass(Cinema.class);
        factory = configuration.buildSessionFactory(registry);


    }

    public SessionFactory getFactory() {
        return factory;
    }
}
