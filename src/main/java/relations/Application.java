package relations;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import relations.domain.Award;
import relations.domain.Cinema;
import relations.domain.Details;
import relations.domain.Film;

import java.util.Collection;

public class Application {

    public static void main(String[] args) {
        HibernateConfiguration configuration = new HibernateConfiguration();
        configuration.configure();
        SessionFactory factory = configuration.getFactory();

        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();

        Film film = new Film("23");
        Details details = new Details(film, 6);
        film.setDetails(details);
        film.getAwards().add(new Award("Tefi"));
        film.getAwards().add(new Award("Oscar"));

        film.getCinemas().add(new Cinema("ggg"));
        film.getCinemas().add(new Cinema("yyy"));

        session.persist(film);

        tx.commit();
        session.close();

        Session s = factory.openSession();
        s.get(Film.class, 100);
        s.close();

        Session ss = factory.openSession();
//        Cinema cinema = ss.get(Cinema.class, 50);
//        System.out.println(cinema.getName());
//        Collection<Film> films = cinema.getFilms();
//        films.forEach(f -> System.out.println(f.getName()));

        Film f = s.get(Film.class, 100);
        System.out.println(f.getName());
        Collection<Cinema> cinemas = f.getCinemas();
        cinemas.forEach(cinema -> System.out.println(cinema.getName()));

        ss.close();
    }
}
