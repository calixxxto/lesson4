package relations.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "films")
@Getter
@Setter
@NoArgsConstructor
public class Film {

    @Id
    @SequenceGenerator(name = "film_id_generator", sequenceName = "film_id_seq", initialValue = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "film_id_generator")
    @Column(name = "film_id")
    private int filmId;

    @Column(unique = true, nullable = false)
    private String name;

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.REMOVE },
                        fetch = FetchType.EAGER)
    @JoinColumn(name = "film_id")
    private Details details;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "film_id")
    private Collection<Award> awards;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "films_in_cinemas",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "cinema_id")
    )
    private Collection<Cinema> cinemas;

    public Film(String name) {
        this.name = name;
        this.awards = new ArrayList<>();
        this.cinemas = new ArrayList<>();
    }
}
