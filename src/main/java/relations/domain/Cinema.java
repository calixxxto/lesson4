package relations.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Getter
@Setter
@Entity
@Table(name = "cinema")
@NoArgsConstructor
public class Cinema {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cinema_id_generator")
    @SequenceGenerator(name = "cinema_id_generator", sequenceName = "cinema_id_seq", initialValue = 50)
    @Column(name = "cinema_id")
    private int cinemaId;

    @ManyToMany
    private Collection<Film> films;

    private String name;

    public Cinema(String name) {
        this.name = name;
        this.films = new ArrayList<>();
    }
}
