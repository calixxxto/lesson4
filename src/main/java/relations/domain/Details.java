package relations.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "film_details")
@Getter
@Setter
@NoArgsConstructor
public class Details {

    @OneToOne
    private Film film;

    @Id
    @GeneratedValue
    @Column(name = "details_id")
    private int detailsId;

    private double duration;

    public Details(Film film, double duration) {
        this.film = film;
        this.duration = duration;
    }
}
