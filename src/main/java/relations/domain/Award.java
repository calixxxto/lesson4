package relations.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "awards")
@Getter
@Setter
@NoArgsConstructor
public class Award {

    @Id
    @GeneratedValue
    private int id;

    @Column(nullable = false)
    private String name;

    public Award(String name) {
        this.name = name;
    }


}
