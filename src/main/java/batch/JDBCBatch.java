package batch;

import java.sql.*;
import java.util.Arrays;

public class JDBCBatch {

    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/films_batch?useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true";
        Connection connection = DriverManager.getConnection(url, "root", "root");

        DatabaseMetaData metaData = connection.getMetaData();
        System.out.println("Is supporting batch: " + metaData.supportsBatchUpdates());

        String query = "insert into film (name) value (?)";
        PreparedStatement statement = connection.prepareStatement(query);

        for (int i = 0; i < 30; i++) {
            statement.setString(1, "Film " + i);
            statement.addBatch();
        }

        int[] rows = statement.executeBatch();
        Arrays.stream(rows).forEach(System.out::println);
    }
}
