package batch;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

// ACID
public class HibernateBatch {

    private static SessionFactory factory;

    public static void main(String[] args) {
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();

        String filName = "film name";

        for (int i = 0; i < 100; i++) {
            filName += " " + i;
            session.persist(filName);

            if (i % 20 == 0) {
                session.flush();
                session.clear();
            }
        }

        tx.commit();
        session.close();
    }
}
