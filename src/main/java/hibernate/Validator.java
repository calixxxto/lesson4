package hibernate;

public class Validator {

    public Validator() {

    }

    public void checkNotEmpty(String value){
        if(value == null || value.trim().length() == 0){
            throw new IllegalArgumentException("Value must be not null");
        }
    }
}
