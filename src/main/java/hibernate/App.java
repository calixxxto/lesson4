package hibernate;

import hibernate.domain.Film;
import hibernate.repository.FilmRepository;
import hibernate.repository.impl.FilmRepositoryImpl;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class App {

    public static void main(String[] args) {
        SessionFactory factory = HibernateUtils.getSessionFactory();

        FilmRepository filmRepository = new FilmRepositoryImpl(factory);
        filmRepository.create(15, "Матрица", 156, "Фантастика", 200);

        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        List<Film> films = session
                .createQuery("from Film", Film.class)
                .getResultList();
        films.forEach(System.out::println);

        tx.commit();
        session.close();

        factory.close();
    }

}
