package hibernate.repository.impl;

import hibernate.Validator;
import hibernate.domain.Film;
import hibernate.repository.FilmRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class FilmRepositoryImpl implements FilmRepository {

    private SessionFactory factory;
    private Validator validator;

    public FilmRepositoryImpl(SessionFactory factory) {
        this.factory = factory;
    }

    //    public FilmRepositoryImpl(SessionFactory factory, Validator validator){
//        this.factory = factory;
//        this.validator = validator;
//    }

    @Override
    public Film create(int id, String name, double duration, String genre, int year) {
//        validator.checkNotEmpty(name);

        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();

        Film film = new Film();
        film.setId(id);
        film.setName(name);
        film.setDuration(duration);
        film.setGenre(genre);
        film.setYear(year);

        session.persist(film);

        tx.commit();
        session.close();

        return film;
    }

    @Override
    public Film getById(int id) {
        try (Session session = factory.openSession()) {
            return session.get(Film.class, id);
        }

    }
}
