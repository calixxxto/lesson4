package hibernate.repository;


import hibernate.domain.Film;

public interface FilmRepository {

    Film create(int id, String name, double duration, String genre, int year);

    Film getById(int id);
}
