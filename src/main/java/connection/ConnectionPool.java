package connection;

public interface ConnectionPool {

    ConnectionWrapper openConnection();

    ConnectionWrapper getConnection();

    void leaseConnection(ConnectionWrapper connectionWrapper);
}
