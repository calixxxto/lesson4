package connection.impl;

import connection.ConnectionPool;
import connection.ConnectionWrapper;
import connection.exception.ConnectionPoolException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static connection.configuration.ConnectionPoolConstants.*;

public class DefaultConnectionPool implements ConnectionPool {

    private int counter;

    private Map<Integer, Connection> workingConnection;

    private LinkedList<ConnectionWrapper> idleConnections;

    private Map<String, Object> properties;

    public DefaultConnectionPool(Map<String, Object> properties) {
        this.counter = 0;
        this.properties = properties;
        this.workingConnection = new HashMap<>();
        this.idleConnections = new LinkedList<>();
    }

    @Override
    public ConnectionWrapper openConnection() {
        int maxSize = Integer.parseInt(properties.get(MAX_SIZE_KEY).toString());
        if (idleConnections.size() + workingConnection.size() == maxSize) {
            throw new ConnectionPoolException("exception text");
        }

        String url = (String) properties.get(URL_KEY);
        String user = (String) properties.get(USER_KEY);
        String password = (String) properties.get(PASSWORD_KEY);

        try {
            Connection connection = DriverManager.getConnection(url, user, password);
            workingConnection.put(++counter, connection);
            return new ConnectionWrapper(counter, connection);
        } catch (SQLException e) {
            throw new ConnectionPoolException(e);
        }
    }

    @Override
    public ConnectionWrapper getConnection() {
        return null;
    }

    @Override
    public void leaseConnection(ConnectionWrapper wrapper) {
        workingConnection.remove(wrapper.getId());
        idleConnections.addLast(wrapper);
    }
}
