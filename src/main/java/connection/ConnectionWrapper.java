package connection;

import java.sql.Connection;

public class ConnectionWrapper  {

    private final int id;
    private final Connection connection;

    public ConnectionWrapper(int id, Connection connection) {
        this.id = id;
        this.connection = connection;
    }

    public int getId() {
        return id;
    }

    public Connection getConnection() {
        return connection;
    }
}
