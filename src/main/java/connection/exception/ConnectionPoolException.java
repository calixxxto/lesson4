package connection.exception;

public class ConnectionPoolException extends RuntimeException {

    public ConnectionPoolException (String message) {
        super(message);
    }

    public ConnectionPoolException (Throwable cause) {
        super(cause);
    }
}
