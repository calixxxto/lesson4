package connection.configuration;

import connection.exception.ConnectionPoolException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class ConfigurationReader {

    private static final String CONFIGURATION_FILE_EXTENSION = ".properties";

    public static Map<String, Object> readConfiguration(String filename){
        verifyFileName(filename);

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            Map<String, Object> properties = new HashMap<>();
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.trim().length() != 0) {
                    String[] data = line.split("=", 2);
                    properties.put(data[0], data[1]);
                }
            }

            return properties;
        } catch (IOException exc) {
            throw new ConnectionPoolException(exc);
        }
    }

    private static void verifyFileName(String fileName) {
        if (fileName == null || fileName.trim().length() == 0 || !fileName.endsWith(CONFIGURATION_FILE_EXTENSION)) {
            throw new ConnectionPoolException("File name must be non blank and has extension " + CONFIGURATION_FILE_EXTENSION);
        }
    }
}
