package connection.configuration;

import connection.ConnectionPool;
import connection.exception.ConnectionPoolException;
import connection.impl.DefaultConnectionPool;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static connection.configuration.ConnectionPoolConstants.*;

public class Configuration {

    private static final int DEFAULT_START_SIZE = 5;
    private static final int DEFAULT_INCREASE_SIZE = 5;
    private static final int DEFAULT_MAX_SIZE = 20;

    private Map<String, Object> properties;

    public Configuration configure() {
        return this;
    }

    public Configuration configure(String filename) {
        //read file
        properties = ConfigurationReader.readConfiguration(filename);
        return this;
    }

    public void verify(){
        Collection<String> requiredKeys = Arrays.asList(
                URL_KEY,
                USER_KEY,
                PASSWORD_KEY
        );
        Set<String> keys = properties.keySet();
        boolean isValid = keys.containsAll(requiredKeys) && requiredKeys.stream()
                .filter(key -> properties.get(key) != null)
                .collect(Collectors.toList())
                .isEmpty();

        if (!isValid) {
            throw new ConnectionPoolException("Invalid connection data");
        }
    }

    public ConnectionPool buildConnectionPool(){
        verify();

        properties.putIfAbsent(START_SIZE_KEY, DEFAULT_START_SIZE);
        properties.putIfAbsent(INCREASE_COUNT_KEY, DEFAULT_INCREASE_SIZE);
        properties.putIfAbsent(MAX_SIZE_KEY, DEFAULT_MAX_SIZE);

        return new DefaultConnectionPool(properties);
    }
}
