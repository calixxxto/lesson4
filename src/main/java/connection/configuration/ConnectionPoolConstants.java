package connection.configuration;

import java.sql.Connection;

public final class ConnectionPoolConstants {

    private ConnectionPoolConstants() {}

    public static final String URL_KEY = "database.url";
    public static final String USER_KEY = "database.user";
    public static final String PASSWORD_KEY = "database.password";

    public static final String START_SIZE_KEY = "cp.start.size";
    public static final String INCREASE_COUNT_KEY = "cp.increase.count";
    public static final String MAX_SIZE_KEY = "cp.max.size";

}
