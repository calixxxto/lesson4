package connection.configuration;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, Object> properties = ConfigurationReader.readConfiguration("src/main/resources/connection.pool.properties");

//        for (Map.Entry<String, Object> entry: properties.entrySet()) {
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }

        properties.forEach((k, v) -> System.out.println(k + " " + v));
    }
}
