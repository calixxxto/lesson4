drop database if exists films_db;
create database films_db;

use films_db;
create table if not exists film (
	id int primary key,
    name varchar(255) not null,
    duration double not null,
    genre varchar(100),
    year int
);

create table if not exists film_detail(
	id int primary key,
    comment varchar(500),
    rating double,
    producer varchar(200) not null,
    film_director varchar(200) not null,
    film_id int not null unique,
    foreign key (film_id) references film(id)
);

create table if not exists cinema (
	id int primary key,
    name varchar(100) not null,
	address varchar(200) not null unique,
    cinemaHallCount int
);


create table if not exists award (
	id int primary key,
    year int not null,
    name varchar(200) not null,
    film_id int not null,
    foreign key (film_id) references film(id)
);

create table if not exists film_in_cinema (
	film_id int,
    cinema_id int,
    constraint film_cinema_id_pk primary key (film_id, cinema_id),
    foreign key (film_id) references film(id),
    foreign key (cinema_id) references cinema(id)
);