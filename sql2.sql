use films_db;

insert into film value (1, "Веном", 134, "Фантастика", 2018);
insert into film (id, name, genre, year, duration) value (2, "Побег из Шоушенка", "Драма", 1994, 165);
insert into film values 
	(3, "Звездные воины", 143, "Фантастика", 1980),
	(4, "Властелин колец", 195, "Фэнтези", 2003),
	(5, "Криминальное чтиво", 178, "Криминальнал", 1994);

select * from film;

insert into film_detail values
(1, "Super dich", 3.56, "Кто-то", "Что-то", 1),
(2, "Бомбезно", 8.42, "Лукас", "Лукас", 3),
(3, "Для семейного просмотра", 9.4, "Барри", "Питер Джексон", 4);

select * from film
left join film_detail on film.id = film_detail.film_id;
